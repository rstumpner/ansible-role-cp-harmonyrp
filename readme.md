# Ansible Role for Checkpoint Harmony Reverse Proxy 

This is a Ansible Role Checkpoint Harmony Reverse Proxy Lifecycle. The Role is tested against a CI/CD Pipeline in Gitlab and have some Tags on the Tasks to Impelement some capabilities for a devops style deployment. 

Requirements:
    None

Role Variables:
    See in the defaults Directory

Example Playbook:
```YAML
- hosts: all
  gather_facts: yes
  roles:
     - ansible-role-cp-harmonyrp
```
## Features
#### Build the Checkpoint Harmony Reverse Proxy Configuration 
- Docker Installation
- Docker Run
###### Step by Step Guide

###### Checkpoint Harmony Reverse Proxy Docker Installation

 
License:
    MIT / BSD

Author Information:
roland@stumpner.at